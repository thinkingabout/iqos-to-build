this["TEMPLATES"] = this["TEMPLATES"] || {};

this["TEMPLATES"]["components/games/key-moments"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


for (var i=0; i < steps.length; i++) {
steps[i].i = i;
}
var shuffleSteps = _.shuffle(steps);
var solution = [];
_.each(shuffleSteps, function(el, i) {
solution[el.i] = i;
});
;
__p += '<div class="key-moments__game game-backdrop view-content" data-solution="' +
((__t = ( solution )) == null ? '' : __t) +
'">';
 if (timer === true) { ;
__p += '\n<div class="d-flex justify-content-center w-100">\n<div class="key-moments__game-timer col d-flex justify-content-center align-items-center">\n<div class="key-moments__game-timer-dots d-flex">\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n</div><h3 class="key-moments__game-countdown text-center">00:00</h3><div class="key-moments__game-timer-dots d-flex flex-row-reverse">\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n<span></span>\n</div>\n</div>\n</div>\n';
 } ;
__p += '<div class="key-moments__game-wrapper"><div class="key-moments__draggables">\n';
 for (var i=0; i < shuffleSteps.length; i++) { ;
__p += '\n<div class="key-moments__draggables-item" data-value>\n<p class="text-center font-italic">\n' +
((__t = ( shuffleSteps[i].text )) == null ? '' : __t) +
'\n</p>\n<span>\n<img src="' +
((__t = ( shuffleSteps[i].img )) == null ? '' : __t) +
'" />\n</span>\n<div class="key-moments__draggables-trigger"></div>\n</div>\n';
 } ;
__p += '\n</div><div class="key-moments__cues">\n';
 for (var i=0; i < shuffleSteps.length; i++) { ;
__p += '\n<div class="key-moments__cues-item" data-label="' +
((__t = ( Globals.locals.views.keymoments[type].game["step_" + (i + 1)].label )) == null ? '' : __t) +
'"></div>\n';
 } ;
__p += '\n</div>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["components/modal/modal-content_game-result"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<header class="modal-header"></header><div class="modal-body modal-body__game-result text-center d-flex flex-column justify-content-center align-items-center flex-grow-1">';
 if (typeof subtitle_html != 'undefined' && subtitle_html != "") { ;
__p += '\n<h1 class="text-uppercase text-danger font-italic">' +
((__t = ( title_html )) == null ? '' : __t) +
'</h1>\n<h5 class="text-uppercase font-italic">' +
((__t = ( subtitle_html )) == null ? '' : __t) +
'</h5>\n';
 } else { ;
__p += '\n<h1 class="text-uppercase font-italic">' +
((__t = ( title_html )) == null ? '' : __t) +
'</h1>\n';
 } ;

 if (typeof text_html != 'undefined' && text_html != "") { ;
__p += '\n<h6 class="text-center font-italic mt-3">' +
((__t = ( text_html )) == null ? '' : __t) +
'</h6>\n';
 } ;

 if (typeof response != 'undefined') { ;
__p += '\n<div class="d-flex w-50 modal-body__game-result-score justify-content-between align-items-center mt-5">\n';
 if (typeof response.your_score != 'undefined') { ;
__p += '\n<h5 class="d-flex flex-column justify-content-center align-items-center text-center text-uppercase mx-auto" data-title="' +
((__t = ( Globals.locals.views.keymoments.motor.game.modal.your_score )) == null ? '' : __t) +
'">' +
((__t = ( response.your_score )) == null ? '' : __t) +
'</h5>\n';
 } ;
__p += '\n';
 if (typeof response.your_time != 'undefined') { ;
__p += '\n<h5 class="d-flex flex-column justify-content-center align-items-center text-center text-uppercase mx-auto" data-title="' +
((__t = ( Globals.locals.views.keymoments.motor.game.modal.your_time )) == null ? '' : __t) +
'">' +
((__t = ( response.your_time )) == null ? '' : __t) +
'</h5>\n';
 } ;
__p += '\n';
 if (typeof response.total_score != 'undefined') { ;
__p += '\n<h5 class="d-flex flex-column justify-content-center align-items-center text-center text-uppercase mx-auto" data-title="' +
((__t = ( Globals.locals.views.keymoments.motor.game.modal.total_score )) == null ? '' : __t) +
'">' +
((__t = ( response.total_score )) == null ? '' : __t) +
'</h5>\n';
 } ;
__p += '\n</div>\n';
 } ;
__p += '\n</div><footer class="modal-footer">\n<div class="container-fluid">\n<div class="row justify-content-center">\n';

var nCols = "col-2";switch (Globals.currentLanguage) {
case "es":
case "fr":
case "it":
case "de":
nCols = "col-3"
break;
case "ru":
nCols = "col-4"
break;
};
__p += '\n';
 if (!(typeof ncorrect != 'undefined' && ncorrect == 4)) { ;
__p += '\n<div class="' +
((__t = ( nCols )) == null ? '' : __t) +
' d-flex justify-content-center">\n<a class="btn btn-block btn-outline-light try-again-btn" href="#" data-dismiss="modal" data-click-sound="tryagain">' +
((__t = ( Globals.locals.general.try_again_btn )) == null ? '' : __t) +
'</a>\n</div>\n';
 } ;
__p += '\n<div class="' +
((__t = ( nCols )) == null ? '' : __t) +
' d-flex justify-content-center">\n';
var _next;
var _nextLabel = Globals.locals.general.next_btn;switch (HashRouter.getPath()[0]) {
case Globals.KEYMOMENTS_MOTOR_GAME_URL:
_next = Globals.KEYMOMENTS_PRODUCT_INTRO_URL;
break;
case Globals.KEYMOMENTS_PRODUCT_GAME_URL:
_next = Globals.INTHEZONE_MOTOR_INTRO_URL;
if (ncorrect < 4) {
_next = Globals.KEYMOMENTS_PRODUCT_VIDEO_URL;
_nextLabel = Globals.locals.views.keymoments.product.game.feedback.error.answer_btn
}
break;
case Globals.INTHEZONE_MOTOR_GAME_URL:
_next = Globals.INTHEZONE_PRODUCT_INTRO_URL;
break;
case Globals.INTHEZONE_PRODUCT_GAME_URL:
_next = Globals.INTHEZONE_PRODUCT_OUTRO_URL;
break;
case Globals.TECHRAY_MOTOR_GAME_URL:
_next = Globals.TECHRAY_PRODUCT_INTRO_URL;
break;
case Globals.TECHRAY_PRODUCT_GAME_URL:
_next = Globals.TECHRAY_MOTOR_INTRO_URL;
break;
}
;
__p += '\n<a class="btn btn-block btn-outline-light next-btn" href="' +
((__t = ( _next )) == null ? '' : __t) +
'" data-click-sound="next">' +
((__t = ( _nextLabel )) == null ? '' : __t) +
'</a>\n</div>\n</div>\n</div>\n</footer>';

}
return __p
};

this["TEMPLATES"]["components/modal/modal-content_techray-game-hotspot"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-body modal-body__techray-game-hotspot text-left d-flex flex-column justify-content-start align-items-start flex-grow-1">\n<img src="' +
((__t = ( bg )) == null ? '' : __t) +
'" /><div class="modal-body__techray-game-hotspot-desc-wrapper pt-5 pl-5 w-50">\n<div class="modal-body__techray-game-hotspot-desc w-100">\n<h5 class="text-danger text-uppercase mb-2">' +
((__t = ( title )) == null ? '' : __t) +
'</h5>\n<h6>' +
((__t = ( text )) == null ? '' : __t) +
'</h6>\n</div>\n</div>\n</div><footer class="modal-footer modal-footer__techray-game-hotspot">\n<div class="container-fluid">\n<div class="row justify-content-center">\n<div class="col-2 d-flex justify-content-center">\n<button type="button" class="btn btn-block btn-outline-light next-btn" data-dismiss="modal" data-click-sound="back">' +
((__t = ( Globals.locals.general.back_btn )) == null ? '' : __t) +
'</button>\n</div>\n</div>\n</div>\n</footer>';

}
return __p
};

this["TEMPLATES"]["components/modal/modal-structure"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal fade' +
((__t = ( classes != 'undefined' ? (' ' + classes) : '' )) == null ? '' : __t) +
'" tabindex="-1" role="dialog" aria-hidden="true">\n<div class="modal-dialog' +
((__t = ( vAlign == 'center' ? ' modal-dialog-centered' : '' )) == null ? '' : __t) +
'" role="document">\n<div class="modal-content"></div>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/inthezone/motor/game"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="inthezone-motor-game-view" class="view game-backdrop"><div class="view-bg">\n</div><img id="inthezone-motor-game-view__zoom-light-effect" src="resources/inthezone/motor/game/zoom-light.png"/><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center"><header class="view-header view-header-toolbar d-flex justify-content-center align-items-center text-center">\n<h6>' +
((__t = ( Globals.locals.views.inthezone.motor.game.header_html )) == null ? '' : __t) +
'</h6>\n</header><div class="view-content d-flex justify-content-between w-100 h-100">\n<div id="speedometer-container">\n<span id="speedometer-pointer"></span>\n</div><button id="inthezone-motor-game-view__rev-btn" class="align-self-end" touch-action="none">\n<img src="./resources/inthezone/motor/game/rev_btn_shadow.png" />\n</button>\n</div><div class="container-fluid view-footer">\n<div class="row justify-content-center">\n<div class="col-2 d-flex justify-content-center">\n<button id="inthezone-motor-game__play-btn" class="btn w-100 btn-outline-light play-btn" data-click-sound="click">' +
((__t = ( Globals.locals.general.play_btn )) == null ? '' : __t) +
'</button>\n</div>\n</div>\n</div>\n</div>' +
((__t = ( window.TEMPLATES["components/modal/modal-structure"]({
vAlign: "center",
classes: "game-feedback-modal"
}) )) == null ? '' : __t) +
'</div>';

}
return __p
};

this["TEMPLATES"]["views/inthezone/motor/intro"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div id="inthezone-motor-intro-view" class="view game-intro">\n<div class="view-bg"></div><div class="d-flex w-100 h-100 flex-column justify-content-end align-items-center"><div class="container-fluid view-content mb-5">\n<div class="row justify-content-center">\n<div class="col-12 mb-5">\n<h1 class="text-center font-italic text-uppercase mb-5">' +
((__t = ( Globals.locals.views.inthezone.motor.intro.title )) == null ? '' : __t) +
'</h1>\n';
 var _w = Globals.currentLanguage == "jp" ? '' : 'w-75' ;
__p += '\n<h4 class="mx-auto ' +
((__t = ( _w )) == null ? '' : __t) +
' text-center">' +
((__t = ( Globals.locals.views.inthezone.motor.intro.text_html )) == null ? '' : __t) +
'</h4>\n</div><div class="col-2 d-flex justify-content-center">\n<button class="btn w-100 btn-outline-light" data-href="' +
((__t = ( Globals.INTHEZONE_MOTOR_GAME_URL )) == null ? '' : __t) +
'" data-click-sound="next">' +
((__t = ( Globals.locals.general.start_btn )) == null ? '' : __t) +
'</button>\n</div>\n</div>\n</div>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/inthezone/product/game"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="inthezone-product-game-view" class="view game-backdrop">\n<div class="view-bg view-bg__rg">\n<img src="resources/inthezone/product/game/tool.png" /><span id="inthezone-product-game-view__tongue">\n<img src="resources/inthezone/product/game/01.png" />\n<img id="inthezone-product-game-view__golden-tongue" src="resources/inthezone/product/game/02.png" />\n<img id="inthezone-product-game-view__fire-tongue" src="resources/inthezone/product/game/03.png" />\n</span>\n</div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center"><span id="inthezone-product-game__stop-btn" touch-action="none" data-click-sound="heat_tap"></span><header class="view-header view-header-toolbar d-flex justify-content-center align-items-center">\n<h6 class="text-center">' +
((__t = ( Globals.locals.views.inthezone.product.game.header_html )) == null ? '' : __t) +
'</h6>\n</header><div class="container-fluid flex-grow-1 view-content">\n<div class="row">\n<div class="col">\n<div id="inthezone-product-game__needle-wrapper"><div id="inthezone-product-game__scale">\n<img src="resources/inthezone/product/game/scale.png" /><span class="h6 font-italic">900° C</span>\n<span class="h6 font-italic">350° C</span>\n<span class="h6 font-italic">300° C</span>\n<span class="h6 font-italic">0° C</span>\n<span class="h6 text-center text-uppercase">' +
((__t = ( Globals.locals.views.inthezone.product.game.perfect_temperature_html )) == null ? '' : __t) +
'</span>\n</div><span id="inthezone-product-game__needle"></span>\n</div>\n</div>\n</div>\n</div><div class="container-fluid view-footer">\n<div class="row justify-content-center">\n<div class="col-2 d-flex justify-content-center">\n<button id="inthezone-product-game__play-btn" class="btn w-100 btn-outline-light play-btn mt-3" touch-action="none" data-click-sound="click">\n' +
((__t = ( Globals.locals.general.play_btn )) == null ? '' : __t) +
'\n</button>\n</div>\n</div>\n</div>\n</div>' +
((__t = ( window.TEMPLATES["components/modal/modal-structure"]({
vAlign: "center",
classes: "game-feedback-modal"
}) )) == null ? '' : __t) +
'</div>';

}
return __p
};

this["TEMPLATES"]["views/inthezone/product/intro"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div id="inthezone-product-intro-view" class="view game-intro">\n<div class="view-bg">\n<p class="d-flex align-items-center justify-content-center pl-5 pr-5 w-100 text-uppercase text-center">' +
((__t = ( Globals.locals.views.inthezone.product.intro.warning_html )) == null ? '' : __t) +
'</p>\n</div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center"><div class="container-fluid view-header mb-5">\n<div class="row justify-content-center">\n<div class="col-12 mb-5">\n';
 var _clss = Globals.currentLanguage == "jp" ? 'mx-auto w-75 ' : '' ;
__p += '\n<h4 class="' +
((__t = ( _clss )) == null ? '' : __t) +
'text-center">' +
((__t = ( Globals.locals.views.inthezone.product.intro.text_html )) == null ? '' : __t) +
'</h4>\n</div><div class="col-2 d-flex justify-content-center">\n<button class="btn w-100 btn-outline-light" data-href="' +
((__t = ( Globals.INTHEZONE_PRODUCT_GAME_URL )) == null ? '' : __t) +
'" data-click-sound="next">' +
((__t = ( Globals.locals.views.inthezone.product.intro.btn )) == null ? '' : __t) +
'</button>\n</div>\n</div>\n</div>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/inthezone/product/outro"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div id="inthezone-product-outro-view" class="view">\n<div class="view-bg"></div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center">\n<header class="container-fluid view-header">\n<div class="row justify-content-center">\n<div class="col-12 mb-5 d-flex justify-content-center">\n<h6 class="text-center w-100">' +
((__t = ( Globals.locals.views.inthezone.product.outro.header_html )) == null ? '' : __t) +
'</h6>\n</div><div class="col-2 d-flex justify-content-center">\n';

let _next = Globals.TECHRAY_MOTOR_INTRO_URL;
if (UserInfo.numOfGamePlayed == 6) {
_next = Globals.USERNAME_URL;
}
;
__p += '\n<button class="btn w-100 btn-outline-light" data-href="' +
((__t = ( _next )) == null ? '' : __t) +
'" data-click-sound="next">' +
((__t = ( Globals.locals.general.next_btn )) == null ? '' : __t) +
'</button>\n</div>\n</div>\n</header>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/keymoments/motor/game"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="keymoments-motor-game-view" class="view">\n<div class="view-bg view-bg__rg-grid"></div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center"><header class="view-header view-header-toolbar d-flex justify-content-center align-items-center">\n<h6 class="text-center">' +
((__t = ( Globals.locals.views.keymoments.motor.game.header_html )) == null ? '' : __t) +
'</h6>\n</header><div class="game-container">\n' +
((__t = ( KeymomentsMotorGame.GAME_TEMPLATE )) == null ? '' : __t) +
'\n</div><div class="container-fluid view-footer">\n<div class="row justify-content-center">\n<div class="col-2 d-flex justify-content-center">\n<button id="keymoments-motor__play-btn" class="btn w-100 btn-outline-light play-btn done-btn" data-click-sound="click">\n' +
((__t = ( Globals.locals.general.play_btn )) == null ? '' : __t) +
'\n</button>\n</div>\n</div>\n</div>\n</div>' +
((__t = ( window.TEMPLATES["components/modal/modal-structure"]({
vAlign: "center",
classes: "game-feedback-modal"
}) )) == null ? '' : __t) +
'</div>';

}
return __p
};

this["TEMPLATES"]["views/keymoments/motor/intro"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="keymoments-motor-intro-view" class="view game-intro">\n<div class="view-bg"></div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center"><div class="container-fluid view-header">\n<div class="row justify-content-center">\n<div class="col-12 mb-5">\n<h2 class="text-center text-uppercase">' +
((__t = ( Globals.locals.views.keymoments.motor.intro.title )) == null ? '' : __t) +
'</h2>\n<h4 class="mx-auto w-50 text-center">' +
((__t = ( Globals.locals.views.keymoments.motor.intro.text_html )) == null ? '' : __t) +
'</h4>\n</div><div class="col-2 d-flex justify-content-center">\n<button class="btn w-100 btn-outline-light" data-href="' +
((__t = ( Globals.KEYMOMENTS_MOTOR_GAME_URL )) == null ? '' : __t) +
'" data-click-sound="next" >' +
((__t = ( Globals.locals.general.start_btn )) == null ? '' : __t) +
'</button>\n</div>\n</div>\n</div>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/keymoments/product/game"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="keymoments-product-game-view" class="view">\n<div class="view-bg view-bg__rg-grid"></div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center"><header class="view-header view-header-toolbar d-flex justify-content-center align-items-center">\n<h6 class="text-center">' +
((__t = ( Globals.locals.views.keymoments.product.game.header_html )) == null ? '' : __t) +
'</h6>\n</header><div class="game-container">\n' +
((__t = ( KeymomentsProductGame.GAME_TEMPLATE )) == null ? '' : __t) +
'\n</div><footer class="container-fluid view-footer">\n<div class="row justify-content-center">\n<div class="col-2 d-flex justify-content-center">\n<button id="keymoments-product__play-btn" class="btn w-100 btn-outline-light play-btn done-btn" data-click-sound="click">' +
((__t = ( Globals.locals.general.play_btn )) == null ? '' : __t) +
'</button>\n</div>\n</div>\n</footer>\n</div>' +
((__t = ( window.TEMPLATES["components/modal/modal-structure"]({
vAlign: "center",
classes: "game-feedback-modal"
}) )) == null ? '' : __t) +
'\n</div>';

}
return __p
};

this["TEMPLATES"]["views/keymoments/product/intro"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="keymoments-product-intro-view" class="view game-intro">\n<div class="view-bg">\n<p class="d-flex align-items-center justify-content-center pl-5 pr-5 w-100 text-uppercase text-center">' +
((__t = ( Globals.locals.views.inthezone.product.intro.warning_html )) == null ? '' : __t) +
'</p>\n</div><div class="d-flex w-100 h-100 flex-column justify-content-start align-items-center"><div class="container-fluid view-header">\n<div class="row justify-content-center">\n<div class="offset-3 col-9 mb-5">\n<h4 class="mx-auto w-75 text-center">' +
((__t = ( Globals.locals.views.keymoments.product.intro.text_html )) == null ? '' : __t) +
'</h4>\n</div><div class="offset-3 col-2 d-flex justify-content-center">\n<button class="btn w-100 btn-outline-light" data-href="' +
((__t = ( Globals.KEYMOMENTS_PRODUCT_GAME_URL )) == null ? '' : __t) +
'" data-click-sound="next">' +
((__t = ( Globals.locals.general.start_btn )) == null ? '' : __t) +
'</button>\n</div>\n</div>\n</div>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/keymoments/product/video_outro"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div id="keymoments-product-video-outro-view" class="view">\n<div class="view-bg"></div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center">\n<header class="container-fluid view-header">';

var btnCols = "col-2";
var txtCols = "w-50";
if (Globals.currentLanguage == "ru" ||
Globals.currentLanguage == "de") {
btnCols = "col-3"
txtCols = "w-75";
}if (Globals.currentLanguage == "jp") {
txtCols = "w-75";
}
;
__p += '<div class="row justify-content-center">\n<div class="col-12 mb-5 d-flex justify-content-center">\n<h6 class="text-center ' +
((__t = ( txtCols )) == null ? '' : __t) +
'">' +
((__t = ( Globals.locals.views.keymoments.product.video_outro.header_html )) == null ? '' : __t) +
'</h6>\n</div><div class="' +
((__t = ( btnCols )) == null ? '' : __t) +
' d-flex justify-content-center">\n<button class="btn w-100 btn-outline-light" data-href="' +
((__t = ( Globals.KEYMOMENTS_PRODUCT_GAME_URL )) == null ? '' : __t) +
'" data-click-sound="tryagain">' +
((__t = ( Globals.locals.general.play_again_btn )) == null ? '' : __t) +
'</button>\n</div>\n';

var _next = Globals.INTHEZONE_MOTOR_INTRO_URL;
if (UserInfo.numOfGamePlayed == 6) {
_next = Globals.USERNAME_URL;
}
;
__p += '\n<div class="' +
((__t = ( btnCols )) == null ? '' : __t) +
' offset-3 d-flex justify-content-center">\n<button class="btn w-100 btn-outline-light" data-href="' +
((__t = ( _next )) == null ? '' : __t) +
'" data-click-sound="next">' +
((__t = ( Globals.locals.general.next_btn )) == null ? '' : __t) +
'</button>\n</div>\n</div>\n</header>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/keymoments/product/video"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="keymoments-product-video-view" class="view">\n<div class="view-bg"></div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center"><header class="view-header view-header-toolbar d-flex justify-content-center align-items-center">\n<h6>' +
((__t = ( Globals.locals.views.keymoments.product.video.header_html )) == null ? '' : __t) +
'</h6>\n</header><div class="view-content d-flex flex-grow-1 justify-content-center w-75 mx-auto">\n<video autoplay muted playsinline poster="' +
((__t = ( Globals.locals.views.keymoments.product.video.poster )) == null ? '' : __t) +
'">\n<source src="' +
((__t = ( Globals.locals.views.keymoments.product.video.url )) == null ? '' : __t) +
'" type=\'video/mp4\'>\n</video>\n</div>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/leaderboard/leaderboard-list"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="leaderboard-list-view" class="view">\n<div class="view-bg view-bg__blur"></div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center"><header class="view-header view-header-toolbar d-flex justify-content-center align-items-center">\n<h3>' +
((__t = ( Globals.locals.views.leaderboard.list.header_html )) == null ? '' : __t) +
'</h3>\n</header><div class="view-content d-flex flex-grow-1 justify-content-center w-75 mx-auto">\n<ul class="list-group w-100 text-uppercase" data-empty-text="' +
((__t = ( Globals.locals.views.leaderboard.list.empty_list_message )) == null ? '' : __t) +
'"></ul>\n</div><footer class="container-fluid view-footer">\n<div class="row justify-content-center">\n<div class="col-2 d-flex justify-content-center">\n<a id="leaderboard-list__restart-btn" class="btn btn-block btn-outline-light" href="#" data-click-sound="next">' +
((__t = ( Globals.locals.general.restart_btn )) == null ? '' : __t) +
'</a>\n</div>\n</div>\n</footer>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/leaderboard/username"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="usernname-view" class="view">\n<div class="view-bg view-bg__blur"></div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center">\n<span></span><div class="container-fluid view-content">\n<div class="row justify-content-center">\n<div class="col-12 d-flex justify-content-center mb-5">\n<h4 class="text-center text-uppercase">' +
((__t = ( Globals.locals.views.leaderboard.username.cta_html )) == null ? '' : __t) +
'</h4>\n</div><form class="col-5">\n<div class="form-group w-100">\n<input id="username-input" type="text" class="form-control form-control-lg text-center w-100 text-uppercase mb-1" maxlength="30" autocapitalize="off" autocorrect="off" autocomplete="off" capitalize="on" aria-describedby="usernameHelp" placeholder="Type here...">\n<label for="username-input" class="text-center text-uppercase w-100">' +
((__t = ( Globals.locals.views.leaderboard.username.your_name_label )) == null ? '' : __t) +
'</label>\n</div>\n</form>\n</div>\n</div><footer class="container-fluid view-footer">\n<div class="row justify-content-center">\n<div class="col-2 d-flex justify-content-center">\n<a class="btn btn-block btn-outline-light" href="#' +
((__t = ( Globals.LEADERBOARD_LIST_URL )) == null ? '' : __t) +
'" data-click-sound="next">' +
((__t = ( Globals.locals.general.skip_btn )) == null ? '' : __t) +
'</a>\n</div><div class="col-2 d-flex justify-content-center">\n<a class="btn btn-block btn-outline-light next-btn" href="#' +
((__t = ( Globals.LEADERBOARD_LIST_URL )) == null ? '' : __t) +
'" data-click-sound="next">' +
((__t = ( Globals.locals.general.next_btn )) == null ? '' : __t) +
'</a>\n</div>\n</div>\n</footer>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/select/games"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var keymoments_default_img = window.Globals.vehicleSelected == "bike" ? "keymoments_bike_default.png" : "keymoments_car_default.png";
var keymoments_active_img = window.Globals.vehicleSelected == "bike" ? "keymoments_bike_active.png" : "keymoments_car_active.png";
;
__p += '<div id="select-activity-view" class="view">\n<div class="view-bg view-bg__rg"></div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center"><div class="container-fluid view-header">\n<div class="row justify-content-center">\n<div class="col-12">\n<h2 class="text-center font-italic text-uppercase">' +
((__t = ( Globals.locals.views.selection.games.cta_html )) == null ? '' : __t) +
'</h2>\n</div>\n</div>\n</div><div class="btn-group btn-group-toggle w-100 view-content" data-toggle="buttons">\n<div class="container-fluid">\n<div class="row justify-content-center">\n<div class="col-4 d-flex flex-column align-items-center">\n<label class="btn font-italic text-uppercase" data-href="' +
((__t = ( Globals.KEYMOMENTS_MOTOR_INTRO_URL )) == null ? '' : __t) +
'">\n<img src="resources/select/games/' +
((__t = ( keymoments_default_img )) == null ? '' : __t) +
'" />\n<img src="resources/select/games/' +
((__t = ( keymoments_active_img )) == null ? '' : __t) +
'" />\n<input type="radio" name="options" autocomplete="off" checked>\n<span>' +
((__t = ( Globals.locals.views.selection.games.games.key_moments.name_html )) == null ? '' : __t) +
'</span>\n</label><p class="text-center font-italic w-75">\n' +
((__t = ( Globals.locals.views.selection.games.games.key_moments.text_html )) == null ? '' : __t) +
'\n</p>\n</div><div class="col-4 d-flex flex-column align-items-center">\n<label class="btn font-italic text-uppercase" data-href="' +
((__t = ( Globals.INTHEZONE_MOTOR_INTRO_URL )) == null ? '' : __t) +
'">\n<img src="resources/select/games/inthezone_default.png" />\n<img src="resources/select/games/inthezone_active.png" />\n<input type="radio" name="options" autocomplete="off">\n<span>' +
((__t = ( Globals.locals.views.selection.games.games.in_the_zone.name_html )) == null ? '' : __t) +
'</span>\n</label><p class="text-center font-italic w-75">\n' +
((__t = ( Globals.locals.views.selection.games.games.in_the_zone.text_html )) == null ? '' : __t) +
'\n</p>\n</div><div class="col-4 d-flex flex-column align-items-center">\n<label class="btn font-italic text-uppercase" data-href="' +
((__t = ( Globals.TECHRAY_MOTOR_INTRO_URL )) == null ? '' : __t) +
'">\n<img src="resources/select/games/techray_default.png" />\n<img src="resources/select/games/techray_active.png" />\n<input type="radio" name="options" autocomplete="off">\n<span>' +
((__t = ( Globals.locals.views.selection.games.games.tech_ray.name_html )) == null ? '' : __t) +
'</span>\n</label><p class="text-center font-italic w-75">\n' +
((__t = ( Globals.locals.views.selection.games.games.tech_ray.text_html )) == null ? '' : __t) +
'\n</p>\n</div>\n</div>\n</div>\n</div><div class="container-fluid view-footer">\n<div class="row justify-content-center">\n<div class="col-2 d-flex justify-content-center">\n<button id="select-activity-view__back-btn" class="btn w-100 btn-outline-light" data-href="' +
((__t = ( Globals.SELECT_LANGUAGE_URL )) == null ? '' : __t) +
'">\n' +
((__t = ( Globals.locals.general.back_btn )) == null ? '' : __t) +
'\n</button>\n</div><div class="offset-6 col-2 d-flex justify-content-center">\n<button id="select-activity-view__next-btn" class="btn w-100 btn-outline-light disabled" data-href="">\n' +
((__t = ( Globals.locals.general.next_btn )) == null ? '' : __t) +
'\n</button>\n</div>\n</div>\n</div>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/select/language"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="select-language-view" class="view">\n<div class="view-bg view-bg__rg"></div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center"><div class="container-fluid view-header">\n<div class="row justify-content-center">\n<div class="col-12">\n<h2 class="text-center font-italic text-uppercase">Select<br>language</h2>\n</div>\n</div>\n</div><div class="container-fluid view-content">\n<div class="row justify-content-center">\n<div class="col-3">\n<div id="select-language-view__languages-list" class="list-group text-center w-100">\n<button class="list-group-item list-group-item-action" type="button" data-toggle="list" data-lang="de" data-click-sound="select_language">Deutsch</button>\n<button class="list-group-item list-group-item-action" type="button" data-toggle="list" data-lang="en" data-click-sound="select_language">English</button>\n<button class="list-group-item list-group-item-action" type="button" data-toggle="list" data-lang="es" data-click-sound="select_language">Español</button>\n<button class="list-group-item list-group-item-action" type="button" data-toggle="list" data-lang="fr" data-click-sound="select_language">Français</button>\n<button class="list-group-item list-group-item-action" type="button" data-toggle="list" data-lang="it" data-click-sound="select_language">Italiano</button>\n<button class="list-group-item list-group-item-action" type="button" data-toggle="list" data-lang="jp" data-click-sound="select_language">Japanese</button>\n<button class="list-group-item list-group-item-action" type="button" data-toggle="list" data-lang="ru" data-click-sound="select_language">Russian</button>\n</div>\n</div>\n</div>\n</div><div class="container-fluid view-footer">\n<div class="row justify-content-center">\n<div class="col-2 d-flex justify-content-center">\n<button id="select-language-view__next-btn" class="btn w-100 btn-outline-light" type="button" data-click-sound="next">OK</button>\n<button id="select-language-view__motor-modal-btn" type="button" class="btn btn-secondary d-flex justify-content-center align-items-center" data-toggle="modal">\n<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">\n<path d="M478.21 334.093L336 256l142.21-78.093c11.795-6.477 15.961-21.384 9.232-33.037l-19.48-33.741c-6.728-11.653-21.72-15.499-33.227-8.523L296 186.718l3.475-162.204C299.763 11.061 288.937 0 275.48 0h-38.96c-13.456 0-24.283 11.061-23.994 24.514L216 186.718 77.265 102.607c-11.506-6.976-26.499-3.13-33.227 8.523l-19.48 33.741c-6.728 11.653-2.562 26.56 9.233 33.037L176 256 33.79 334.093c-11.795 6.477-15.961 21.384-9.232 33.037l19.48 33.741c6.728 11.653 21.721 15.499 33.227 8.523L216 325.282l-3.475 162.204C212.237 500.939 223.064 512 236.52 512h38.961c13.456 0 24.283-11.061 23.995-24.514L296 325.282l138.735 84.111c11.506 6.976 26.499 3.13 33.227-8.523l19.48-33.741c6.728-11.653 2.563-26.559-9.232-33.036z"/>\n</svg>\n</button>\n</div>\n</div>\n</div>\n</div><div id="select-language-view__motor-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">\n<div class="modal-dialog modal-dialog-centered" role="document">\n<div class="modal-content">\n<div class="modal-body">\n<div class="w-100 d-flex flex-column align-items-stretch">\n<h6 class="mb-1">Leaderboard reset</h6>\n<p class="color-red mb-3">Take care, reset operations are <strong>irreversible</strong>.</p><form id="reset-f1-leaderboard-form" class="form-inline w-100 mb-3">\n<label for="reset-f1-leaderboard-input" class="w-100 text-left">Car Leaderboard</label>\n<div class="input-group mr-sm-2 flex-grow-1">\n<div class="input-group-prepend">\n<div class="input-group-text">Code:<strong id="reset-f1-code" class="ml-1"></strong></div>\n</div>\n<input id="reset-f1-leaderboard-input" type="text" class="form-control" placeholder="Digit here..." maxlength="6" autocomplete="off" autocapitalize="off" autocorrect="off">\n</div><button type="submit" class="btn btn-primary">Reset</button>\n<small id="reset-f1-leaderboard-input-help" class="form-text w-100 mb-2" data-default-message="Type the code in the input field."></small>\n</form><form id="reset-motogp-leaderboard-form" class="form-inline w-100">\n<label for="reset-motogp-leaderboard-input" class="w-100 align-self-left text-left">Motorcycle Leaderboard</label>\n<div class="input-group mr-sm-2 flex-grow-1">\n<div class="input-group-prepend">\n<div class="input-group-text">Code:<strong id="reset-motogp-code" class="ml-1"></strong></div>\n</div>\n<input id="reset-motogp-leaderboard-input" type="text" class="form-control" placeholder="Digit here..." maxlength="6" autocomplete="off" autocapitalize="off" autocorrect="off">\n</div><button type="submit" class="btn btn-primary">Reset</button>\n<small id="reset-motogp-leaderboard-input-help" class="form-text w-100 mb-2" data-default-message="Type the code in the input field."></small>\n</form>\n</div><hr /><div class="d-flex flex-column align-items-center">\n<h6 class="mb-3 text-left w-100">Select vehicle type</h6>\n<div class="w-100 d-flex flex-column align-items-stretch">\n<div class="btn-group btn-group-vertical btn-group-toggle w-100 mb-3" data-toggle="buttons">\n<label class="btn btn-lg d-flex justify-content-start align-items-center">\n<input id="car-radio-btn" type="radio" name="motor-type-radio" value="car" autocomplete="off">\n</label><label class="btn btn-lg d-flex justify-content-start align-items-center">\n<input id="bike-radio-btn" type="radio" name="motor-type-radio" value="bike" autocomplete="off">\n</label>\n</div><button id="select-language-view__motor-modal-save-btn" type="button" class="btn btn-primary w-25 mx-auto">Save</button>\n</div>\n</div></div>\n</div>\n</div>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/techray/motor/game"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="techray-motor-game-view" class="view">\n<div class="view-bg"></div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center">\n<header class="view-header view-header-toolbar d-flex justify-content-center align-items-center">\n<h6 class="text-center">' +
((__t = ( Globals.locals.views.techray.motor.game.header_html )) == null ? '' : __t) +
'</h6>\n</header><div class="view-content">\n<div id="techray-motor-game-view__360-viewer" touch-action="none">\n<ul id="techray-motor-game-view__hotspots-list"></ul>\n<img id="techray-motor-game-view__img">\n</div>\n</div><footer class="container-fluid view-footer">\n<div class="row justify-content-center">\n<div class="col-2 d-flex justify-content-center">\n<a class="btn w-100 btn-outline-light btn-next" href="#' +
((__t = ( Globals.TECHRAY_PRODUCT_INTRO_URL )) == null ? '' : __t) +
'" data-click-sound="next">' +
((__t = ( Globals.locals.general.next_btn )) == null ? '' : __t) +
'</a>\n</div>\n</div>\n</footer>\n</div>' +
((__t = ( window.TEMPLATES["components/modal/modal-structure"]({
vAlign: "center",
classes: "game-feedback-modal"
}) )) == null ? '' : __t) +
'\n</div>';

}
return __p
};

this["TEMPLATES"]["views/techray/motor/intro"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div id="techray-motor-intro-view" class="view game-intro">\n<div class="view-bg"></div><div class="container-fluid view-header">\n<div class="row justify-content-center">\n<div class="col-12 mb-5">\n<h2 class="text-center text-uppercase">' +
((__t = ( Globals.locals.views.techray.motor.intro.title )) == null ? '' : __t) +
'</h2>\n';

var text = Globals.locals.views.techray.motor.intro.f1_text_html;
if (Globals.vehicleSelected == "bike") {
text = Globals.locals.views.techray.motor.intro.motogp_text_html;
}var _w = (Globals.currentLanguage == "jp") ? "w-75" : "w-50";
;
__p += '\n<h6 class="mx-auto ' +
((__t = ( _w )) == null ? '' : __t) +
' text-center">' +
((__t = ( text )) == null ? '' : __t) +
'</h6>\n</div>';

var cols = (Globals.currentLanguage == "it") ? "col-4" : "col-3";
;
__p += '<div class="' +
((__t = ( cols )) == null ? '' : __t) +
' d-flex justify-content-center">\n<button class="btn w-100 btn-outline-light" data-href="' +
((__t = ( Globals.TECHRAY_MOTOR_GAME_URL )) == null ? '' : __t) +
'" data-click-sound="next">' +
((__t = ( Globals.locals.general.look_inside_btn )) == null ? '' : __t) +
'</button>\n</div>\n</div>\n</div>\n</div>';

}
return __p
};

this["TEMPLATES"]["views/techray/product/game"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="techray-product-game-view" class="view"><div class="view-bg"></div><div class="d-flex w-100 h-100 flex-column justify-content-between align-items-center">\n<header class="view-header view-header-toolbar d-flex justify-content-center align-items-center">\n<h6 class="text-center">' +
((__t = ( Globals.locals.views.techray.product.game.header_html )) == null ? '' : __t) +
'</h6>\n</header><div class="view-content">\n<div id="techray-motor-game-view__360-viewer" touch-action="none">\n<ul id="techray-motor-game-view__hotspots-list"></ul>\n<img id="techray-motor-game-view__img" src="" >\n</div>\n</div><footer class="container-fluid view-footer">\n<div class="row justify-content-center">\n<div class="col-2 d-flex justify-content-center">\n<a class="btn w-100 btn-outline-light btn-next" href="#' +
((__t = ( Globals.TECHRAY_PRODUCT_INTRO_URL )) == null ? '' : __t) +
'" data-click-sound="next">' +
((__t = ( Globals.locals.general.next_btn )) == null ? '' : __t) +
'</a>\n</div>\n</div>\n</footer>\n</div>' +
((__t = ( window.TEMPLATES["components/modal/modal-structure"]({
vAlign: "center",
classes: "game-feedback-modal"
}) )) == null ? '' : __t) +
'\n</div>';

}
return __p
};

this["TEMPLATES"]["views/techray/product/intro"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div id="techray-product-intro-view" class="view game-intro">\n<div class="view-bg"></div><div class="container-fluid view-header">\n<div class="row justify-content-center">\n<div class="col-12 mb-5">\n<h2 class="text-center text-uppercase">' +
((__t = ( Globals.locals.views.techray.product.intro.title )) == null ? '' : __t) +
'</h2>\n';

var txtCols = Globals.currentLanguage.match("de|jp") ? "w-75" : "w-50";
;
__p += '\n<h6 class="mx-auto ' +
((__t = ( txtCols )) == null ? '' : __t) +
' text-center">' +
((__t = ( Globals.locals.views.techray.product.intro.text_html )) == null ? '' : __t) +
'</h6>\n</div>\n';

var cols = (Globals.currentLanguage == "it") ? "col-4" : "col-3";
;
__p += '\n<div class="' +
((__t = ( cols )) == null ? '' : __t) +
' d-flex justify-content-center">\n<button class="btn w-100 btn-outline-light" data-href="' +
((__t = ( Globals.TECHRAY_PRODUCT_GAME_URL )) == null ? '' : __t) +
'" data-click-sound="next">' +
((__t = ( Globals.locals.general.look_inside_btn )) == null ? '' : __t) +
'</button>\n</div>\n</div>\n</div>\n</div>';

}
return __p
};